import pandas

tmp = pandas.read_csv("data/2018_Central_Park_Squirrel_Census_-_Squirrel_Data.csv")

print(tmp.head(9))
print(tmp.info())
# count(fur_color)
color_count = tmp["Primary Fur Color"].value_counts()
color_count_dict = color_count.to_dict()

print(color_count_dict)