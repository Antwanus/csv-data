import pandas
import pandas as pd
import turtle

df = pd.read_csv("50_states.csv")
image = "blank_states_img.gif"

screen = turtle.Screen()
screen.title("USA States game")
screen.addshape(image)
turtle.shape(image)


def get_coords_by_state(state):
    record = df[df.state == state]
    return int(record.x), int(record.y)


states_to_guess_list = df["state"].tolist()
copy_all_states = states_to_guess_list.copy()
print(states_to_guess_list)

while len(states_to_guess_list) != 0:
    answer_state = screen.textinput(
        title=f"[ {(len(states_to_guess_list) - 50) * -1} / 50 ]",
        prompt="What's another state to name?"
    ).title()

    # exit & saves to CSV
    if answer_state == "Exit":
        print('bye')
        temp = {'state': states_to_guess_list}
        tmp = pandas.DataFrame(temp)
        tmp.to_csv("states_to_learn.csv")
        guessed_states = [state for state in states_to_guess_list if state not in copy_all_states]
        print(guessed_states)
        break
    # answer found
    elif answer_state in states_to_guess_list:
        states_to_guess_list.remove(answer_state)
        print(states_to_guess_list)
        t = turtle.Turtle()
        t.penup()
        t.hideturtle()
        t.goto(get_coords_by_state(answer_state))
        t.write(answer_state)
    # not found
    else:
        print(answer_state, " is not correct")

guessed_states = [state for state in copy_all_states if state not in states_to_guess_list]
print(guessed_states)

# def prompt_user():
#     return screen.textinput(title="Guess a state..", prompt="What's another state?")
#
#
# found_states = []
#
#
# def f():
#     counter = 0
#     while counter < 3:
#         counter += 1
#         answer = prompt_user()
#         found_row = df[df["state"] == answer]
#         if answer in found_states:
#             print(answer, "-> already guessed")
#             f()
#         elif found_row.empty:
#             print(answer, "-> not found")
#             f()
#         else:
#             print(answer, "-> found!")
#             t = turtle.Turtle()
#             t.penup()
#             t.hideturtle()
#             t.goto(int(found_row["x"]), int(found_row["y"]))
#             t.write(found_row["state"].item())
#             found_states.append(answer)
#             f()
#         print(counter)
#
#
# f()

# answer_state = prompt_user()
# query = df[df["state"] == answer_state]
#
# if query.empty:
#     print("NOT FOUND:", answer_state)
#     answer_state = prompt_user()
# else:
#     print(query)
#     answer_state = prompt_user()

# Finding x, y on the bg pic
# def get_mouse_click_coordinates(x, y):
#     print(x, y)
# turtle.onscreenclick(get_mouse_click_coordinates)
# turtle.mainloop()

screen.exitonclick()
