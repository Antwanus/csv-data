# with open("weather_data.csv") as data:
#     data_list = data.readlines()
#     for e in data_list:
#         print(e)

# import csv
#
# with open("weather_data.csv") as file:
#     reader_obj = csv.reader(file)
#     print(reader_obj)
#     temps = []
#     for row in reader_obj:
#         print(row)
#         temps.append(row[1])
#     temps = temps[1:]
import pandas

dataframe = pandas.read_csv('weather_data.csv')

# df_dict = dataframe.to_dict()
# print(df_dict)

# df_temp_list = dataframe["temp"].to_list()
# print(df_temp_list)

# # total = 0
# # for temp in df_temp_list:
# #     total += int(temp)
# total = sum(df_temp_list)
#
# avg_temp = int(total / len(df_temp_list))
# print(avg_temp)

# avg_temp = int(dataframe["temp"].mean())
# print(avg_temp)
#
# max_temp = dataframe["temp"].max()
# print(max_temp)
#
# print(dataframe["condition"])
# print(dataframe.condition)

# print(dataframe, '\n')
#
# # GET ROW DATA
# print(dataframe[dataframe['condition'] == 'Sunny'])
# print(dataframe[dataframe['temp'] == dataframe['temp'].max()])
#
# monday = dataframe[dataframe['day'] == 'Monday']
# print(monday)
# print(monday['temp'] * 9 / 5 + 32)

# CREATE DATAFRAME FROM SCRATCH
data_dict = {
    'students': ['Amy', 'Barry', 'Charlie'],
    'scores': [100, 20, 30]
}

data = pandas.DataFrame(data_dict)
data.tocsv('new_csv.csv')
